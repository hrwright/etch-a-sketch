var dimension;

var containerSize=600;
var numSquares=dimension*dimension;

$(document).ready(function(){
  reset();

})

function makeSquares(){
  for(var i=0; i<dimension; i++){
    for(var j=0; j<dimension; j++){
      $(".container").append("<div class='square'></div>");
    }
  }
  squareDimention=(containerSize/dimension);
  $(".square").css({"width":squareDimention, "height": squareDimention})
}

function changeColor(s){
  var r1=parseInt(Math.random()*10).toString();
  var r2=parseInt(Math.random()*10).toString();
  var g1=parseInt(Math.random()*10).toString();
  var g2=parseInt(Math.random()*10).toString();
  var b1=parseInt(Math.random()*10).toString();
  var b2=parseInt(Math.random()*10).toString();
  var color="#"+r1+r2+g1+g2+b1+b2;
  if($(s).css("background-color") != "rgb(00, 00, 255)"){
    $(s).css("background-color", color);
  }
}

function reset(){
  $(".square").remove();
  setDimension();
  makeSquares();
  $(".square").hover(function(){
      changeColor($(this));
  })
}

function setDimension(){
  dimension=prompt("Enter a number between 16 and 100.");
  while(dimension > 100){
    dimension=prompt("Takes too long to load, choose a smaller number.");
  }
}
